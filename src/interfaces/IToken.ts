/**
 * Интерфейс, описывающий структуру токена.
 */
interface IToken {
    /**
     * Токен доступа.
     */
    access_token: string;
    /**
     * Истекает.
     */
    expires_in: number;
    /**
     * Тип токена.
     */
    token_type: string;
    /**
     * ТОкен сброса.
     */
    refresh_token ?: string
}

export default IToken;