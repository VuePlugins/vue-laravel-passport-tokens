/**
 * Интерфейс, описывающий структуру конфига аутентификации.
 */
interface IAuthConfig {
    /**
     * ID Клиента.
     */
    client_id: number;
    /**
     * Тип авторизации (password и др.).
     */
    grant_type: string;
    /**
     * Ключ авторизации.
     */
    client_secret: string;
    /**
     * Скоупы для запроса определенной группы прав.
     */
    scope?: string;
}

export default IAuthConfig;