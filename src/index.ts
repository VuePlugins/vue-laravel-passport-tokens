/**
 * Импортируем библиотеку для работы с токенами.
 */
import TokenHelper from "./classes/TokenHelper";

/**
 * Плагин для управления токена Laravel Passport.
 */
const VuePassportToken = {
    /**
     * Инъекция плагина.
     *
     * @param {*} Vue Vue.js
     * @param {*} options Дополнительные параметры.
     */
    install(Vue, options) {
        Vue.mixin({
            mounted() {
                if (!(window as any).wasLoaded) {
                    (window as any).wasLoaded = true;
                    console.log('Laravel passport-tokens plugin was loaded!');
                }
            }
        });

        Vue.prototype.$auth = new TokenHelper();
    }
};

export default VuePassportToken;
