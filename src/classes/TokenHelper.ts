/**
 * Структура токена.
 */
import IToken from "../interfaces/IToken";
/**
 * Структура конфига для авторизаци.
 */
import IAuthConfig from "../interfaces/IAuthConfig";

/**
 * Класс для работы с токенами.
 */
class TokenHelper implements IToken {
    /**
     * Токен доступа.
     */
    access_token: string;
    /**
     * Истекает.
     */
    expires_in: number;
    /**
     * Токен сброса.
     */
    refresh_token: string;
    /**
     * Тип токена.
     */
    token_type: string;
    /**
     * Конфиг с настройками для получения токена (авторизации).
     */
    private authConfig: IAuthConfig;

    /**
     * Авторизация пользователя.
     *
     * @param {number} client_id ID клиента для получения токенов.
     * @param {string} client_secret Секретный токен клиента.
     * @param {string} grant_type Тип авторизации.
     * @param {string} scope Скоуп прав.
     * @returns {any}
     */
    authUser(
        client_id: number,
        client_secret: string,
        grant_type = 'password',
        scope?: string
    ) {
        if (typeof client_id !== 'number') return new Error('Параметр client_id должен быть числом!');
        if (typeof client_secret !== 'string' || client_secret.length < 1) return new Error('Параметр client_secret должен быть не пустой строкой!');
        if (typeof grant_type !== 'string'|| grant_type.length < 1) return new Error('Параметр grant_type должен быть не пустой строкой!');
        if (typeof scope !== 'undefined' && (typeof scope !== 'string' || scope.length < 1)) return new Error('Параметр scope может быть не обязательным или должен быть не пустой строкой!');

        return true;
    }

    /**
     * Установка токенов.
     *
     * @param {string} access_token
     * @param {int} expires_in
     * @param token_type
     * @param {string} refresh_token
     */
    setToken(
        access_token : string,
        expires_in : number,
        token_type = 'Bearer',
        refresh_token ?: string
    ) {

        const results = [
            this.setAccessToken(access_token),
            this.setTokenExpire(expires_in),
            this.setTokenType(token_type),
            this.setRefreshToken(refresh_token)
        ];

        const errors : Error[] = [];

        results.forEach(result => {
            if (result instanceof Error) {
                console.warn(result);
                return errors.push(result);
            }
        });

        return errors.length > 0 ? errors[0] : true;
    }

    /**
     * Устанавливает токен доступа.
     *
     * @param {string} access_token Токен доступа.
     * @returns {any}
     */
    setAccessToken(access_token : string) {
        if (typeof access_token !== 'string' || access_token.length < 1) {
            return new Error('Переданное значение должно быть строкой и не быть пустой!');
        } else {
            this.access_token = access_token;
            localStorage.setItem('access_token', access_token);
            return true;
        }
    }

    /**
     * Устанавливает тип токена.
     *
     * @param {string} token_type Тип токена.
     * @returns {any}
     */
    setTokenType(token_type: string) {
        if (typeof token_type !== 'string' || token_type.length < 1) {
            return new Error('Переданное значение должно быть строкой и не быть пустой!');
        } else {
            this.token_type = token_type;
            localStorage.setItem('token_type', token_type);
            return true;
        }
    }

    /**
     * Устанавливает время жизни токена.
     *
     * @param {number} expire_time Время жизни.
     * @returns {any}
     */
    setTokenExpire(expire_time: number) {
        if (typeof expire_time !== 'number') {
            return new Error('Переданное значение должно быть числом!');
        } else {
            this.expires_in = expire_time + +new Date();
            localStorage.setItem('expires_in', String(this.expires_in));
            return true;
        }
    }

    /**
     * Устанавливает токен сброса.
     *
     * @param refresh_token Токен сброса.
     * @returns {any}
     */
    setRefreshToken(refresh_token) {
        if (typeof refresh_token !== 'undefined' && (typeof refresh_token !== 'string' || refresh_token.length < 1)) {
            return new Error('Переданное значение может быть не обязательным или должно быть строкой и не быть пустой!');
        } else {
            this.refresh_token = refresh_token;
            localStorage.setItem('refresh_token', refresh_token);
            return true;
        }
    }

    /**
     * Проверить авторизованность (наличие установленного токена и время его жизни).
     */
    isAuthenticated() {
        return !this.tokenWasExpired() && !!this.getToken()
    }

    /**
     * Получить токен доступа.
     */
    getToken() {
        if (!this.access_token) {
            const from_cache = localStorage.getItem('access_token');
            return from_cache ? from_cache : false;
        }
        return this.access_token;
    }

    /**
     * Проверить, истекло ли время жизни токена.
     */
    tokenWasExpired() {
        let expire_date;
        expire_date = !this.expires_in ? Number(localStorage.getItem('expires_in')) : this.expires_in;
        return !expire_date? false : new Date() >= expire_date;
    }

    /**
     * Удалить токены из хранилища.
     */
    removeTokens() {
        this.access_token = undefined;
        this.expires_in = undefined;
        this.refresh_token = undefined;
        this.token_type = undefined;

        localStorage.removeItem('access_token');
        localStorage.removeItem('expires_in');
        localStorage.removeItem('refresh_token');
        localStorage.removeItem('token_type');
    }
}

export default TokenHelper;